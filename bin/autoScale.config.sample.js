'use strict';

module.exports = [
  // [ "worker", "formula(optional)", "min(optional)", "max(optional)" ],
  ["generate", nproc => nproc / 2, 1,],  // nproc/2; but at least 1
  ["distribute", nproc => nproc < 2 ? 0 : 1],  // none for t2.micro; 1 otherwise
  ["scene2Hls", nproc => nproc < 2 ? 0 : 1],  // none for t2.micro; 1 otherwise
  ["bulkjob", nproc => nproc < 2 ? 0 : 1],  // none for t2.micro; 1 otherwise
  ["videoEvent", nproc => nproc < 2 ? 0 : 1],  // none for t2.micro; 1 otherwise
];
