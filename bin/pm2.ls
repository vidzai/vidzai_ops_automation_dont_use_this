#!/usr/bin/env node

function ohNo(...args) {
  console.error(...args);
  process.exit(1);
}

function usage(...args) {
  if (args.length > 0)
     console.error(...args);
  ohNo(`usage:
lists "pm2 ls" in plain text
${process.argv[1]} [--|--help]
--: to pipe "pm2 jlist" explicitly to format
--help: for this message
By default, internally invokes "pm2 jlist" and parses to produce output`);
}

function slurpStdin() {
  const fs = require('fs'); 
  const MAX = 1 << 16;
  const buf = Buffer.alloc(MAX);

  const len = fs.readSync(0, buf, 0, MAX);
  if (len >= MAX) {
    ohNo(MAX, "too low");
  }

  let s = buf.toString('utf8');
  s = s.slice(0, len).trim();
  if (s.length == 0) {
    ohNo("got no input");
  }

  return s;
}

function slurpCmd(file, ...args) {
  const { execFileSync } = require('child_process');

  console.error("running", ...arguments);
  let out = execFileSync(file, args, {
    timeout: 3000,  // 1 sec
    encoding: 'utf8',
		//stdio: [ 'inherit', 'pipe', 'inherit' ],
  });
  return out;
}

// console.log(">>", process.argv.join("|"));
let source = (() => slurpCmd("pm2", "jlist"));
switch(process.argv.length) {
  case 2:
     break;
  case 3:
     {
        let opt = process.argv[2];
        switch(opt) {
        case "--":
          source = slurpStdin;
          break;
        case "--help":
          usage();
          break;
        default:
          usage("unknown option", opt);
        }
      }
      break;
   default:
      usage("invalid arguments", process.argv);
}

function pad(s, left=true) {
  const PAD = '            ';
  if (left)
    return `${s}${PAD}`.slice(0, PAD.length);
  else
    return `${PAD}${s}`.slice(-PAD.length);
}

function toMb(num, decPlaces=2) {
  return (num/(1.0*(1<<20))).toFixed(decPlaces);
}

function output(...args) {
  console.log(args.map(arg=>pad(arg)).join(''));
}

let s;
try {
  s = source();
  if ((!s) || s.length <= 0)
    ohNo("got no input");

  const list = JSON.parse(s);

  output("pid", "name", "status", "memory", "cpu", "pm2.id");
  for (const e of list) {
    output(e.pid, e.name, e.pm2_env.status, `${toMb(e.monit.memory)} mb`, `${e.monit.cpu}%`, e.pm2_env.pm_id);
  }
} catch (e) {
  console.error("error", e, `'${s}'`);
}
