const cwd=`${__dirname}/..`;

function configuration(options) {
  const commonOptions = {
  };
  return { ...commonOptions, ...options, "cwd": cwd };
};

module.exports = {
  apps: [
    configuration({
      name: "autoScale",
      script: `autoScale`,
      autorestart: false, // run just once on reboot
      args: "--wait=1000",  // wait for 1 second before acting
    }),
  ]
}
