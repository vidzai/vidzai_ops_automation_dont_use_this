/**
 * This is a pm2 config file to start top monitoring
 * Since this is started/stopped manually, this is kept separate from main ecospace.config.js file
 */
module.exports = {
  apps: [
    {
      name: "top",
      script: `top`,
      args: "-b -u vidzai -d 10 -c -w 132", // batch, show vidzai's only, every 10sec, full cmdline, 132 chars
    },
  ]
}
