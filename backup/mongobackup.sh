:

# https://wiki.bash-hackers.org/howto/getopts_tutorial

CMD=$0
usage()
{
  echo "$@
usage: $0 [-s] -b bucket database
Back up mongo database to a specified AWS S3 bucket
If -s is specified, it will shutdown mongo server before doing backup.
  The shutdown might help in avoiding 'out of memory' issues.
  The server is restarted after backup.
All collections are backed up." 1>&2
  exit 1
}

bucket=
shutdown=
while getopts "sb:" opt; do
  case $opt in
    b)
      bucket=$OPTARG
      shift $((OPTIND-1))
      ;;
    s)
      shutdown=Y
      ;;
    \?)
      usage "Invalid option: -$OPTARG"
      ;;
    :)
      usage "Option -$OPTARG requires an argument."
      ;;
    
  esac
done

if [ -z $bucket ] ; then
  usage "bucket not specified"
fi

database=
case $# in
  0 ) usage "database not specified"
    ;;
  * ) database=$1; shift
    ;;
esac

manageServices()
{
  # manageServices start|stop
  [ "$shutdown" = "Y" ] || {
    return
  }
  
  echo "${1}ing services"
  systemctl $1  rabbitmq-server.service 
  systemctl $1  mysqld 

  case $1 in
  stop )
    sleep 60
    ;;
  esac
}

# Customize these to the MongoDB DB setup
user="vidzaiprod"
password="vidzai123"
#uri="mongodb+srv://$user:$password@cluster0-vidzai-analytics.1cocw.mongodb.net/$database"
uri="mongodb://$user:$password@3.136.111.168:8090/$database"
TEMP=/tmp/${database}.dump
OUTPUTFILE=$TEMP.tar.gz
S3URI="s3://${bucket}/backup/"

#pre-requisite
# Assumes that aws configure has been run and setup
# Below files should exist, and be readable by the owner
# File 1: ~\.aws\credentials 
# [default]
# aws_access_key_id = <access key>
# aws_secret_access_key = <secret-access-key>
# File 2: ~\.aws\config 
# [default]
# region = ap-south-1

# For customizing mongodump command, refer to https://docs.atlas.mongodb.com/command-line-tools/#connect-with-mongodump
{
  manageServices stop &&
  # /usr/bin/mongodump -h $host -d $database -o $TEMP &&
  /usr/bin/mongodump --uri="$uri" --numParallelCollections=1 -o $TEMP &&
  tar -zcvf $OUTPUTFILE $TEMP &&
  aws s3 cp $OUTPUTFILE $S3URI &&
  echo "Successfully copied $OUTPUTFILE to $S3URI" 1>&2 &&
  rm -rf $OUTPUTFILE $TEMP && 
  manageServices start &&
  echo "Backup of $database $@ successful" 1>&2
} || {
  echo "Backup of $database $@ failed" 1>&2
  manageServices start || echo "starting services failed" 1>&2
  exit 1
}
