:

# https://wiki.bash-hackers.org/howto/getopts_tutorial

CMD=$0
usage()
{
  echo "$@
usage: $0 -b bucket database [tables]
Back up database to a specified AWS S3 bucket
All tables are backed up by default; pass table names to subset some tables." 1>&2
  exit 1
}

bucket=
while getopts "r:b:" opt; do
  case $opt in
    b)
      bucket=$OPTARG
      shift $((OPTIND-1))
      ;;
    \?)
      usage "Invalid option: -$OPTARG"
      ;;
    :)
      usage "Option -$OPTARG requires an argument."
      ;;
    
  esac
done

if [ -z $bucket ] ; then
  usage "bucket not specified"
fi

database=
case $# in
  0 ) usage "database not specified"
    ;;
  * ) database=$1; shift
    ;;
esac

if [ $# -gt 0 ]; then
  echo "Only $@ tables will be backed up" 1>&2
fi

# Customize these to the MySQL DB setup
host="localhost"
port=3306
user="root"
password="DigitalVidzai1!"

TEMP=/tmp/${database}.dump.sql
S3URI="s3://${bucket}/backup/"

#pre-requisite
# Assumes that aws configure has been run and setup
# Below files should exist, and be readable by the owner
# File 1: ~\.aws\credentials 
# [default]
# aws_access_key_id = <access key>
# aws_secret_access_key = <secret-access-key>
# File 2: ~\.aws\config 
# [default]
# region = ap-south-1

{
  mysqldump --host=$host --port=$port --user=$user --password=$password $database $@ > $TEMP &&
  gzip $TEMP &&
  aws s3 cp $TEMP.gz $S3URI &&
  echo "Successfully copied $TEMP.gz to $S3URI" 1>&2 &&
  rm -f $TEMP.gz && 
  echo "Backup of $database $@ successful" 1>&2
} || {
  echo "Backup of $database $@ failed" 1>&2
  exit 1
}
