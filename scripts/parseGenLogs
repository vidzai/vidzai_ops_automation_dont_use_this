#!/usr/bin/env node
'use strict';

process.on('unhandledRejection', (reason, promise) => {
  console.error("unhandledRejection", ...arguments);
});
process.on('uncaughtException', (err, origin) => {
  console.error(
    `Caught exception: ${err}\n` +
    `Exception origin: ${origin}`
  );
});


const fs = require('fs');
const readline = require('readline');

async function processLineByLine(processor) {
  function _process(line) {
    try {
      processor(line)
    } catch(e) {
      console.error('processing line error (ignored)', e, line);
    }
  }
  const rl = readline.createInterface({
    // input: fs.createReadStream('y.log');
    input: process.stdin,
    crlfDelay: Infinity
  }).on('close', () => {
    _process(null);
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    _process(line);
  }
}


// pattern: [video-3425][2021-07-24T20:43:01.755+05:30][Worker] info: ------------------------------ END Successful @#1 ------------------------------
// const pattern = /\[([\w\d-]+)\]\[(\d+-\d+-\d+T\d+:\d+:\d+(\.\d+)?(\+\d+(:\d+)?)?)\]\[(\w+)\] *(\w+): (.*)/;
const pattern = /\[(?<instance>[\w\d-]+)\]\[(?<ts>\d+-\d+-\d+T\d+:\d+:\d+(\.\d+)?(\+\d+(:\d+)?)?)\]\[(?<class>\w+)\] *(?<logType>\w+): (?<rest>.*)/;
function parseline(line) {
  const matches = line.match(pattern);
  if (!matches)
    return;

  return Object.entries(matches.groups).map(([key, value]) => {
    switch(key) {
      case "ts":
        try {
          value = new Date(value);
        } catch(e) {
          console.warn("to-date", e, value);
        }
    }
    return [ key, value ];
  }).reduce((o, [key, value]) => {
    o[key] = value;
    return o;
  }, {});
}

let genData = {}; // data accumulated for a given generation

const patterns = {
  start: { // [Queue] info: received 32 {"request_id":8744,"options":{}}
    class: "Queue",
    rest: /received\s*\d+\s*.*"request_id"\s*:\s*(?<request_id>\d+).*/
  },
  stop: { // [Worker] info: ---- END Successful @#1 / Failed after... ---
    class: "Worker",
    rest: /-+\s*END\s*(?<result>.*?)\s*-+$/,
  },
  download: { // [Http] info: downloaded https://teststorage.vidzai.com/videos/8apljJtcBekjJa2SMg1MNduLS18Sp3VgqShRRl5W3KS9Q0AFzkQLlkUwnYCXULk95Y6sCKLATedqfjKKcMZmSs.mp4 /home/vidzai/source/vidzai_app/vidgen/8744/523/base.mp4
    class: "Http",
    rest: /downloaded\s+(?<src>\S+)\s+(?<dest>\S+)/
  },
  upload: { // [MediaServer] info: Upload completed {"message_code":4,"message":"MediaVideo added successfully.","statusCode":200,"data":{"id":19191,"file_name":"out.mp4","url":"https://teststorage.vidzai.com/videos/0EQpIevZo7tJ2o3fiAtDW74pwBQKA2MVN2UCJWllsUn5H6m2bz0nHDiKtJjsudKyqGYwuQlBb06nSrXGCFNgBo.mp4","local_path":"/home/vidzai/source/vidzai_app/public/storage/JXDe8VbqND62kICvK4OKjJBmuPlStdjiEXGtMIN2DrLnrRhWfuQy7EGX88qsriMF7qeVN5Rkdaac0NXDg3g3xj.mp4","thumb_nail":27802,"duration":37.53,"width":1280,"height":720,"updatedAt":"2021-07-24T08:32:10.997Z","createdAt":"2021-07-24T08:32:10.997Z"}} validating response
    class: "MediaServer",
    rest: /Upload completed.*"message"\s*:\s*(?<type>\S+)\s+added\s+successfully.*/
  },
  svg2png: { // [Image] info: svg2png successful /home/vidzai/source/vidzai_app/vidgen/8744/523/image/1.png.svg /home/vidzai/source/vidzai_app/vidgen/8744/523/image/1.png
    class: "Image",
    rest: /svg2png\s+successful\s+(?<svg>\S+)\s+(?<png>\S+)/
  },
  dda: { // [SceneComposer] info: DDA generated Funds Opted processing Timer(3.826ms) duration 4.640000000000001
    class: "SceneComposer",
    rest: /DDA generated\s+(?<name>.*)Timer\((?<timer>[^)]+)\)\s*duration\s*(?<duration>[\d\.]+)/,
  },
  ffmpegS: { // [FfmpegCmd] info: END  FFMPEG Command for scene binding
    class: "FfmpegCmd",
    rest: /END\s+FFMPEG\s+Command\s+for\s+scene\s+binding/i,
  },
};
function matchPattern(data) {
  for (const [k, p] of Object.entries(patterns)) {
    if (p.class != data.class)
      continue;
    const matches = data.rest.match(p.rest);
    if (!matches)
      continue;

    return [k, matches.groups];
  }
  return [];
}

function print(values) {
  console.log(values.map(v =>
    (typeof v == "string") ? v.replace(/[,"'\n\r]/g, "?"): v).join(","));
}

let output = {};

let headers = null;
function printOutput() {
  const start = (output.start|| [])[0]; if (!start) return;
  const stop = (output.stop|| [])[0]; if (!stop) throw "stop not found";
  const values = {};
  function add(k, v) {
    values[k] = v;
  }
  add("instance", start.instance);
  add("ts", (start.ts instanceof Date? start.ts.toISOString(): start.ts));
  add("request", start.request_id);
  add("result", stop.result);
  const duration = (stop.ts - start.ts);
  add("duration", duration/1000.0);

  let total = 0;
  for (const k of Object.keys(patterns).filter(k => !(k == "start" || k == "stop"))) {
    const kd = output[k] || [];
    add(`${k}-count`, kd.length);
    const duration = kd.reduce((s, v) => s + v.elapsed, 0);
    total += duration;
    add(`${k}-duration`, duration/1000.0);
  }
  add("factored-duration%", total*100.0/duration);
  
  if (headers == null) {// first line
    headers = Object.keys(values);
    print(headers);
  }
  print(Object.values(values));
}

function processData(data) {
  const [k, matches] = matchPattern(data);
  if (!k)
    return;

  const first = (output["start"]||[])[0];
  if (first && first.instance != data.instance) {
    console.error("instances dont match", first, data);
    return;
  }

  (output[k] || (output[k] = [])).push({ ...data, ...matches });  // combine both

  switch(k) {
    case "stop":
      printOutput();
      output = {};
      break;
  }
}

let lastLine = null;
function processline(line) {
  if (line === null) {
    printOutput();
    process.exit(0);
  }

  const parsed = parseline(line);
  if (!parsed)
    return;

  parsed.elapsed = lastLine? (parsed.ts - lastLine.ts): null;
  processData(parsed);

  lastLine = parsed;
}

processLineByLine(processline).then(() => {}).catch(e => console.error("error", e))
