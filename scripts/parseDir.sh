error() {
	echo "$@
exitting" 1>&2
	exit 1
}
if [ $# -ne 1 ]; then
	error "usage: $0 testdir
Analyzes contents of testdir and creates output files in that dir"
	exit 1
fi
dir=$1; shift
[ -d $dir ] || error "'$dir' doesnt exist or is not a directory"

cd $dir

gunzip *.gz

trace() {
	echo $@ 1>&2
}

trace "parsing generate logs"
cat generate-out*_*.log generate-out-??.log generate-out-?.log generate-out.log |
 fgrep -w '[Worker]' | grep 'process' | parseLogs | egrep '^Elapsed|processed' > g.csv

cat generate-out*_*.log generate-out-??.log generate-out-?.log generate-out.log |
	parseGenLogs > r.csv

cat monitor5-out*.log | parseTop > m.csv

