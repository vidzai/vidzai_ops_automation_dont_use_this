#!/usr/bin/env node
'use strict';

const fs = require('fs');
const readline = require('readline');

async function processLineByLine(processor) {
  const rl = readline.createInterface({
    // input: fs.createReadStream('y.log');
    input: process.stdin,
    crlfDelay: Infinity
  }).on('close', () => {
    processor(null);
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    processor(line);
  }
}


// pattern: [video-3425][2021-07-24T20:43:01.755+05:30][Worker] info: ------------------------------ END Successful @#1 ------------------------------
// const pattern = /\[(?<instance>[\w\d-]+)\]\[(?<ts>\d+-\d+-\d+T\d+:\d+:\d+(\.\d+)?(\+\d+(:\d+)?)?)\]\[(?<class>\w+)\] *(?<logType>\w+): (?<rest>.*)/;
const patterns = {
// top - 16:47:28 up 1 day,  5:51,  1 user,  load average: 0.00, 0.00, 0.00
  top: /top\s*-\s*(?<ts>\d+:\d+:\d+)\s*(?<rest>.*)/,
// %Cpu(s):  3.2 us,  3.2 sy,  0.0 ni, 93.5 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
  cpu: /%Cpu\(s\):\s*(?<us>\S+)\s*us,\s*(?<rest>.*)/,
// KiB Mem :  4040024 total,  2684292 free,   532072 used,   823660 buff/cache
  mem: /KiB\s*Mem\s*:\s*\d+\s*total,\s*(?<free>\d+)\s*free,\s*(?<used>\d+)\s*used,\s*(?<rest>.*)/,

// 10572 vidzai    20   0 1112200 178884  15040 S 123.6  4.4   0:06.19 ffmpeg -i /home/vidzai/source/vidzai_app/vidgen/9352/577/base.+
  process: /^\s*\d+\s+\w+\s+\d+\s+\d+\s+\d+\s+\d+\s+\w+\s+\w+\s+(?<cpu>\S+)\s+(?<mem>\S+)\s+(?<time>\S+)\s+(?<cmd>.+)/,
};
const processes = {
  generate: /node .*apps\/generate\/.*/,
  ffmpeg: /ffmpeg\s*.*/,
  phantom: /.*phantomjs-prebuilt.*/,
  main: /node .*apps\/main-server\/.*/,
};
function matchProcess(cmd) {
  for (const [k, p] of Object.entries(processes)) {
    if (p.test(cmd))
      return k;
  }
}
function processesData(curMatch) {
  const d = {};
  for (const k of Object.keys(processes)) {
    const data = curMatch[k] || {};
    for (const attr of [ 'cpu', 'mem' ]) {
      d[`${k}-${attr}%`] = data[attr] || "";
    }
  }
  return d;
}

function print(values) {
  console.log(values.map(v => 
    (typeof v == "string") ? v.replace(/[,"'\n\r]/g, "?"): v).join(","));
}

let curMatch = {};
let headers = null;
function dumpMatch() {
  if (Object.keys(curMatch).length == 0)
    return;
    
  const values = ({
        time: (curMatch.top||{})["ts"],
        "cpu.us": (curMatch.cpu||{})["us"],
        "mem.free(KB)": (curMatch.mem||{})["free"],
        "mem.used(KB)": (curMatch.mem||{})["used"],
        ...processesData(curMatch),
        "mem...": (curMatch.mem||{})["rest"],
        "cpu...": (curMatch.cpu||{})["rest"],
        "top...": (curMatch.top||{})["rest"],
      });

  if (headers == null) { // first line
    headers = Object.keys(values);
    print(headers);
  }
  print(Object.values(values));
}

function parseline(line, outputFunct) {
  for (const [k, p] of Object.entries(patterns)) {
    const matches = line.match(p);
    if (!matches)
      continue;

    if (k == "top") {
      outputFunct();
      curMatch = {};
    }

    const data = matches.groups;

    if (k == "process") {
      //console.log("process", data);
      const pname = matchProcess(data.cmd);
      if (pname)
        curMatch[pname] = data;
    } else {
      curMatch[k] = data;
    }

    return true;
  }
  return false;
}

function processline(line) {
  if (line === null) {
    dumpMatch();
    process.exit(0);
  }

  parseline(line, dumpMatch);
}

processLineByLine(processline).then(() => {}).catch(e => console.error("error", e))
