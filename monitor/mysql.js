#!/usr/bin/env node
"use strict";

function ohNo(...args) {
  console.error(...args);
  process.exit(1);
}

function usage(...args) {
  ohNo(...args, `
Check MySQL server Connect and Output Results on Console
Send alerts on failure
Usage:
${process.argv[1]} [--help] [--db=database] [--alert]
--help: for this message
--db: database; two values (local/remote); defaults to remote
--alert: send alert even if connect succeeds

Invokes "./alert" to send failure alerts.

Present Limitations:
1. Alert configuration limitations
2. More database configuration limitations`)
}

// get the client
const mysql = require('mysql2');

const argv = require('yargs').argv
if (argv.help) {
  usage();
}


const alert = require('./alert');
 
async function checkDb(dbConfig) {

  function mask(o) {  // mask sensitive fields
    return Object.entries(o).
      map(([k, v]) => [k, /user|password/i.test(k)? "****": v]).
      reduce((o, [k, v]) => { o[k] = v; return o; }, {});
  }

  async function _alert(sub, msg, ... args) {
    try {
      return await alert(sub, msg, mask(dbConfig), ...args);
    } catch(e) {
      console.error("Sending alert failed", e, sub, msg, ...args);
    }
  }

  async function sendAlert(sub, msg, ...args) {
    await _alert(sub, msg, ...args);
    trackCheck(sub, msg, ...args);
  }

  function trackCheck(sub, msg, ...args) {
    console.log("ALERT", sub, msg, mask(dbConfig), ...args);
  }

  async function runQuery(connection, q) {
    const p = new Promise((resolve, reject) => {
      connection.query( q, function(err, results, fields) {
          if (err) {
            reject(err);
            return;
          }

          resolve([results, fields]);
      });
    })
    return await p;
  }

    try {
      // create the connection to database
      const connection = mysql.createConnection(dbConfig);
 
      // simple query
      try {
        const [results, fields] = await runQuery(connection, 'SELECT 0=0 AS truth');
	if (argv.alert)
      	  await sendAlert("MySQL up", "DB Check succeeded", results);
	else
          trackCheck("MySQL up", "DB Check succeeded", results);
        return(true);
      } catch(e) {
        await sendAlert("MySQL down", "DB Check failed", e);
      }
    } catch(e) {
      await sendAlert("MySQL down", "DB Connection failed", e);
    }

    return(false);
}

const Databases = {
  local: {
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'test'
  },
  remote: {
    host: "3.136.111.168",
    user: "root",
    password: "DigitalVidzai1!",
    database: "vidzaidev",
  },
  test: {
    host: "3.136.111.169",
    user: "root",
    password: "DigitalVidzai1!",
    database: "vidzaidev",
  },
};

function selectDb() {
  const sel = argv.db || "remote";
  const db = Databases[sel];
  if (db) return db;
  throw `Invalid DB selection: ${sel}`;
}


function after(rc) { process.exit(!!rc); }
checkDb(selectDb()).then(after, after);
