#!/usr/bin/env node
"use strict";

function getHandshake(config, hid) {
  const [api, anal, id, nonce ] = [ config.api, config.anal, config.id, config.nonce ];
  const tracker = `T${id}T`;

  function statusValidator(statusLambda) {
    function validator(resp) {
        if (!statusLambda(resp.status)) {
          return new Error(`Invalid response status: ${resp.status} ${resp.statusText}`);
        }
    }
    return validator;
  }
    
const HANDSHAKES = {
  play: {
      request: {
        method: "GET",
        url: `${api}/video/${id}/v2/batch?include_analytics=no`,
      },
      validate: resp => {
	if (resp.status < 200 || resp.status >= 300)
          return new Error(`Invalid response status: ${resp.status} ${resp.statusText}`);
	{
	  const type = resp.headers["content-type"];
	  if (!type.startsWith("text/html"))
            return new Error(`Unexpected response type: ${type}`);
	}
	try {
	  let len = resp.data.length;
          if (len < 100)
	    return new Error(`Suspicious response content#${len}: "${resp.data}}`);
	} catch(e) {
	  return new Error(`Suspicious response content type: ${typeof resp.data}`);
	}
      }
  },
  interaction: {
      request: {
        method: "POST",
        url: `${anal}/api/video_request/${id}/intreraction?nonce=${nonce}`,
        data: {
          "action": "AUTH_LANDING_PAGE",
          "data":{
            "id":"-1",
            "interactionName":"VIDEO_PAGE_VIEW",
            "interactionType":"PAGE_EVENT",
            "answerValue":"SEEN"
          },
          "tracker": tracker
        }
      },
    validate: statusValidator(status => status < 300 || status == 500), // Change this when the bug is fixed
  },
};

  return HANDSHAKES[hid];
}

function ohNo(...args) {
  console.error(...args);
  process.exit(1);
}

function usage(...args) {
  ohNo(...args, `\
Check API and Analytics Server Health by
doing specific handshakes
Usage:
${process.argv[1]} [--help] [--alert] [--api=url] [--anal=url] [--id=requestId] [handshake1... handshake2..]
--help: for this message
--alert: send alert even if all succeeds

api/anal: overides the default urls

requestId: override the default test request to test against
handshake1..n: subselection of handshakes; by default all handshakes are tested one by one

Invokes "./alert" to send failure alerts.

Present Limitations:
1. Alert configuration limitations
2. Handshakes to be added
`)
}

const argv = require('yargs').argv
if (argv.help) {
  usage();
}


const alert = require('./alert');
const checkHandshake = require('./checkHandshake');
 
async function checkHandshakes(config, handshakes) {
  if (handshakes.length <= 0)
    handshakes = [ "play", "interaction" ];

  async function _alert(sub, msg, ... args) {
    try {
      return await alert(sub, msg, ...args, "Config:", config);
    } catch(e) {
      console.error("Sending alert failed", e, sub, msg, ...args, "Config:", config);
    }
  }

  async function sendAlert(sub, msg, ...args) {
    await _alert(sub, msg, ...args);
    trackCheck(sub, msg, ...args);
  }

  function trackCheck(sub, msg, ...args) {
    console.log("TRACE", sub, msg, ...args, config);
  }

  let results = [];

  let failed = undefined;

  for (const h of handshakes) {
    let handshake;
    try {
      handshake = getHandshake(config, h);
      if (!handshake)
        throw new Error(`Invalid handshake: ${h}`);

      const res = await checkHandshake(handshake.request, handshake.validate);
      if (res.error) {
        failed = {
          error: res,
          handshake: handshake
        };
        break;
      }

      results.push({ handshake: handshake, response: res.response });

      trackCheck("Handshake succeeded", handshake, (res.reponse || {}).headers);
    } catch(e) {
        failed = {
          error: e,
          handshake: handshake
        };
      break;
    }
  }

  const title = `Video(${argv.name})`

  if (failed) {
    await sendAlert(`${title} down`, "Handshake failure", failed.error, failed.handshake);
    trackCheck(`${title} down`, "Handshake failure", failed.error, failed.handshake);
    return false;
  }

  const details = `Checks:\n${results.map(r => `${r.handshake.request.url} ${r.response.status} ${r.response.statusText}`).join("\n")}`;
	if (argv.alert) {
    await sendAlert(`${title} up`, "Video checks succeeded", details);
  } else {
    trackCheck(`${title} up`, "Video checks succeeded", details);
  }
  return true;
}

const config = {
  api: "https://testapi.vidzai.com",
  anal: "https://testanalytics.vidzai.com",
  id: "h6KiB0TUHN82gKAvaxrYFvtcWirBVe6o9mdqbiFeqbD08b2MtSaDum2i6lTY1Rv4U9faLNmGVLTSQLrIuFG1RG",
  nonce: 1613024938140,
};

function getConfig() {
  for (const p of [ "api", "anal", "id", "nonce" ]) {
    if (argv[p])
      config[p] = argv[p];
  }
  return config;
}

function after(rc) { process.exit(!!rc); }
checkHandshakes(getConfig(), argv._).then(after, rc => { console.error("**",rc); after(rc);});
