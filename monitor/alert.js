"use strict";

const nodemailer = require("nodemailer");
const util = require("util");

const Config = require("./config");

const config = Config.alert;

// async..await is not allowed in global scope, must use a wrapper
async function alert(subj, msg, ...args) {
  if (config.disabled)
    return false;

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport(config.transporter);

  let text = `\
Message: ${msg}
Generated At: ${new Date}`;

  if (args.length > 0) {
    const argsTxt = args.map(arg=>util.format("", arg)).join("\n");
    text = `${text}

Details:
${argsTxt}`;
  }

  // send mail with defined transport object
  let info = await transporter.sendMail({
    ...{
      subject: subj, // Subject line
      text: text, // plain text body
    },
    ...config.message
  });

  console.log("Message sent: %s", info.messageId);

  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

  return true;
}

module.exports = alert;
