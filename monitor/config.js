"use strict";

/**
 * A wrapper over npm-config which allows us access the configuration
 * data structures as an object, which was the legacy way of using it.
 * Static access, since it is a singleton.
 */

const config = require('config');

/**
 * old style of accessing as an object
 */
const o = config.util.toObject();
module.exports = o;
