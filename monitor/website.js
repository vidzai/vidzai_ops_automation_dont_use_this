#!/usr/bin/env node
"use strict";

function getURL(config, hid) {
  const base = config.base;

const URLs = {
  home: `${base}/`,
  products: `${base}/products/`,
  insuranceVideo: `https://youtu.be/x7zAxXqu3kI`,
  healthCare: `${base}/health-care/`,
};

  return URLs[hid];
}

function ohNo(...args) {
  console.error(...args);
  process.exit(1);
}

function usage(...args) {
  ohNo(...args, `\
Check Website Health by
doing specific URLs and Videos
Usage:
${process.argv[1]} [--help] [--alert] [--base=url] [url1... url1..]
--help: for this message
--alert: send alert even if all succeeds

base: overides the default www.vidzai.com

url: the default hardwired url names

Invokes "./alert" to send failure alerts.

Present Limitations:
1. Alert configuration limitations
2. URLs can be arbitrary
`)
}

const argv = require('yargs').argv
if (argv.help) {
  usage();
}

const alert = require('./alert');
const checkHandshake = require('./checkHandshake');
 
async function checkHandshakes(config, handshakes) {
  if (handshakes.length <= 0)
    handshakes = [ "home", "products", "insuranceVideo", "healthCare", ];

  async function _alert(sub, msg, ... args) {
    try {
      return await alert(sub, msg, ...args, "Config:", config);
    } catch(e) {
      console.error("Sending alert failed", e, sub, msg, ...args, "Config:", config);
    }
  }

  async function sendAlert(sub, msg, ...args) {
    await _alert(sub, msg, ...args);
    trackCheck(sub, msg, ...args);
  }

  function trackCheck(sub, msg, ...args) {
    console.log("TRACE", sub, msg, ...args, config);
  }

  let results = [];

  let failed = undefined;

  for (const h of handshakes) {
    try {
      const url = getURL(config, h);
      if (!url)
        throw new Error(`Invalid url: ${h}`);

      const res = await checkHandshake({ url: url, method: "GET" });

      if (res.error) {
        failed = {
          error: res,
          url: url
        };
        break;
      }

      results.push({ url: url, response: res.response });

      trackCheck("Handshake succeeded", url, (res.response || {}).headers);
    } catch(e) {
        failed = {
          error: e,
          url: url
        };
      break;
    }
  }

  const title = `Website`

  if (failed) {
    await sendAlert(`${title} down`, "Handshake failure", failed.error, failed.url);
    trackCheck(`${title} down`, "Handshake failure", failed.error, failed.url);
    return false;
  }

  const details = `Checks:\n${results.map(r => `${r.url} ${r.response.status} ${r.response.statusText}`).join("\n")}\n`;
	if (argv.alert) {
    await sendAlert(`${title} up`, "URL checks succeeded", details);
  } else {
    trackCheck(`${title} up`, "URL checks succeeded", details);
  }
  return true;
}

const config = {
  base: "https://www.vidzai.com",
};

function getConfig() {
  for (const p of Object.keys(config)) {
    if (argv[p])
      config[p] = argv[p];
  }
  return config;
}

function after(rc) { process.exit(!!rc); }
checkHandshakes(getConfig(), argv._).then(after, after);
