// get the client
const axios = require('axios');

/**
 * @validate: should validate response and return undefined if value or Error object otherwise
 * @returns: { error: , response: }. error is set if check fails. response is set if there's a response
 */
async function check(request, validate) {
  let validator = (validate? (resp, err) => validate(resp):
    (resp, err) => (err? Error(`handshake fault: ${err}`): resp));

  let err, resp;
  try {
      resp = await axios(request);
  } catch(e) {
      err = e;
      resp = e.response; // could be undefined
  }

  if (validate && resp) { // override err by validation of response
    try {
       err = validate(resp);
    } catch(e) {
       console.error("Error while validating response", e);
       err = new Error(`Error while validating: ${e}`);
    }
  }
  return {
    error: err,
    response: resp
  };
}
 
module.exports = check;
