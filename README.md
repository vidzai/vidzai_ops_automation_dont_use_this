# Ops Automation

Assorted scripts collection for various miscellaneous purposes
organized as per below directory organization.

Directory | Description
--- | ---
bin | Assorted scripts used in operations
backup | Scripts used in backup cron jobs
monitor | Scripts used in server/service availability monitoring jobs

> For operations (by `vidzai` user), add the `bin` directory to the bash `PATH` environment.

# `bin` Directory Contents

Script | Description
--- | ---
autoScale | A script used to auto-scale worker instances. Intended to be run on reboot by `pm2`. The scaling parameters are presently coded into the script itself.
autoScale.config.js | The worker auto-scale configuration read in by the `autoScale` script. This is intended for customization for each installation and is *not* chked into git.
autoScale.config.sample.js | Sample `autoScale.config.js` provided as reference out-of-the-box.
ops | `pm2` settings for operation scripts. At present, sets up `autoScale` as a `pm2` process.
pm2.ls | Same as `pm2 ls`; but outputs pure text for use in piping to other shell commands
pm2.start | A convenient script to restart all `vidzai` programs after, say, upgrades. For use during development, debugging etc.
setRotate | An annotated script to describe how to setup `pm2` `logrotate`. _For illustrative purposes only_.
takeSnapshots | Take memory heap snapshots of all vidzai processes into a specified folder.

# `monitor` Directory Contents

Most of the scripts here are intended to be setup as cron jobs.

Script | Description
--- | ---
db | Check on db availability. db --help will let u know usage.
